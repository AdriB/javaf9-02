package numbers;

public class TestNumber {
    private TestNumber (){

    }

    public static double computeSum (Number [] numbers){
        double sum=0;
        //for (int index=0;index<numbers.length;index ++)
        for (Number nr:numbers){
           sum +=nr.doubleValue() ;
        }
        return sum;
    }
    //supraincarcarea metodei comute sum
    public static int computeSum (Integer [] numbers){
        int sum=0;
        //for (int index=0;index<numbers.length;index ++)
        for (Integer nr:numbers){
            sum +=nr ;
        }
        return sum;
    }
    public static double computeSum (String numbers,String delimiter){

        String [] nrs= numbers.split(delimiter);
        Integer [] intNumbers= new Integer[nrs.length];
        for (int index=0;index<nrs.length;index++){
            String currentNr=nrs[index];
            intNumbers[index] = Integer.parseInt(currentNr);

        }
        return computeSum(intNumbers);

    }
}
