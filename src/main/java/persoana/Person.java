package persoana;

public class Person {
    private String id;
    private String firstname;
    private int age;
    private String sex;
    private boolean retired;

    public Person(String id, int age) {
        this.id = id;
        this.age = age;

    }

    public String getId() {
        return this.id;
    }



    public void setId(String id) {
        this.id=id;
    }
//
    public boolean isRetired() {
        return retired;
    }

    public void setRetired(boolean retired) {
        this.retired = retired;
    }

}

