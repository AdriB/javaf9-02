package vehicle;

public class Car extends Vehicle {
    private Boolean convertible;


    public Car(int maxSpeed, Boolean convertible) {
        super(maxSpeed);
        this.convertible = convertible;
    }

    public boolean isConvertible() { //unboxing
        return convertible;
    }
    public  int tuneCar (int maxSpeedIncrease){
        return (this.maxSpeed +maxSpeedIncrease);
    }
}
