package vehicle;

public class Vehicle {
    public static final int Default_MAX_SPEED = 50;
    protected final int maxSpeed;//ca sa nu putem crea un obiect fara max speed

    public Vehicle(){
        this (Default_MAX_SPEED);

    }
    public Vehicle(int maxSpeed){
        this.maxSpeed=maxSpeed;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }
}
