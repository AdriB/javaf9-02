package shape;

public class Circle extends Shape {
    //public static final double PI = 3.14;
    private double radius;
    public  Circle(){
        //super (4);
        this(4.5);
        System.out.println("constructor circle");
    }
    public Circle (double radius){
        super(5);
        this.radius=radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double computeAria(){
        return Math.PI *Math.pow(radius,2);
    }
    public void printColorCode(){
        System.out.println("the color"+this.colorCode);
    }

}

